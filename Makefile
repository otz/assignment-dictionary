.PHONY = all clean

C = nasm
CF = -f elf64 -g
L = ld

all: main

main: main.o dict.o lib.o
	${L} -o $@ $^

main.o: words.inc colon.inc

%.o: %.asm
	${C} ${CF} -o $@ $<

clean:
	rm -f main *.o