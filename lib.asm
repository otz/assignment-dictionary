SYS_READ    equ 0
SYS_WRITE   equ 1
SYS_EXIT    equ 60
FD_STDIN    equ 0
FD_STDOUT   equ 1
FD_STDERR   equ 2
C_NEWLINE   equ 0xA  

section .text
global FD_STDIN
global FD_STDOUT
global FD_STDERR
global read_char
global read_word
global read_line
global parse_uint
global parse_int
global string_length
global string_equals
global string_copy
global print_newline
global print_char
global print_string
global print_uint
global print_int
global exit

; Принимает код возврата и завершает текущий процесс
exit:
    mov eax, SYS_EXIT
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor edx, edx
    xor eax, eax
.check_next_char:
    mov dl, [rdi + rax]
    inc rax
    test dl, dl
    jnz .check_next_char
    dec rax
    ret

; Принимает указатель на нуль-терминированную строку и файловый дескриптор
; Выводит строку по назначению
print_string:
    push rdi
    push rsi
    call string_length
    mov rdx, rax
    mov eax, SYS_WRITE
    pop rdi
    pop rsi
    syscall
    ret

; Принимает файловый дескриптор
; Выводит символ перевода строки (0xA)
print_newline:
    mov rsi, rdi
    mov edi, C_NEWLINE

; Принимает код символа и файловый дескриптор
; Выводит символ по назначению
print_char:
    push rdi
    mov rdi, rsi
    mov rsi, rsp
    mov edx, 1
    mov eax, SYS_WRITE
    syscall
    add rsp, 8
    ret

; Выводит знаковое 8-байтовое число в десятичном формате по назначению
print_int:
    test rdi, rdi
    jns print_uint
    neg rdi    
    push rdi
    push rsi
    mov edi, '-'
    call print_char
    pop rsi
    pop rdi

; Выводит беззнаковое 8-байтовое число в десятичном формате по назначению
print_uint:
    sub rsp, 24
    mov ecx, 23
    mov byte [rsp+rcx], 0
    mov r10, 0xCCCCCCCCCCCCCCCD ; 10-reciprocal approximation (*2^3)
    mov rax, rdi
.store_next_digit:
    dec rcx
    mov rdi, rax
    mul r10         ; "Division by invariant integers using multiplication",
    shr rdx, 3      ; T. Granlund, P. L. Montgomery, 1994
    mov rax, rdx
    lea rdx, [rdx+rdx*4]    ; gcc optimized way
    add rdx, rdx            ; to multiply by 10
    sub rdi, rdx
    or dil, '0'
    mov [rsp+rcx], dil
    test rax, rax
    jnz .store_next_digit
.print_stored:
    lea rdi, [rsp+rcx]
    call print_string
    add rsp, 24
    ret

; Принимает два указателя на нуль-терминированные строки
; Возвращает 1 если они равны, 0 иначе
string_equals:
    xor eax, eax
    xor ecx, ecx
    xor edx, edx
.check_next_char:
    mov dl, [rdi+rcx]
    cmp dl, [rsi+rcx]
    jne .distinct
    inc rcx
    test dl, dl
    jnz .check_next_char
.same:
    inc rax
.distinct:
    ret

; Принмает файловый дескриптор
; Читает один символ из источника и возвращает его
; Возвращает 0 если достигнут конец потока
read_char:
    push 0
    mov rsi, rsp
    mov edx, 1
    mov eax, SYS_READ
    syscall
    test al, al   ; 1 on success, 0 on EOF, negative on error
    js .error
    pop rax
    ret
.error:
    add rsp, 8
    xor eax, eax
    ret 

; Принимает адрес начала буфера, размер буфера, файловый дескриптор
; Читает в буфер слово из stdin, пропуская пробельные символы в начале
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx
; Дописывает к слову нуль-терминатор
; При неудаче возвращает 0 в rax
read_word:
%macro JUMP_IF_WHITESPACE 1
    cmp al, 0x20
    je %1
    cmp al, 0x09
    je %1
    cmp al, 0x0A
    je %1
%endmacro
    push rbx
    mov rbx, rdi
    push r12
    xor r12d, r12d
    push r13
    mov r13, rsi
    push r14
    mov r14, rdx
    cmp r12, r13
    je .buffer_overflow
.skip_leading_whitespace:
    mov rdi, r14
    call read_char
    JUMP_IF_WHITESPACE .skip_leading_whitespace
.check_and_read_next:
    test al, al
    jz .done
    mov [rbx+r12], al
    inc r12
    cmp r12, r13
    je .buffer_overflow
    mov rdi, r14
    call read_char
    JUMP_IF_WHITESPACE .done
    jmp .check_and_read_next
.done:
    mov byte [rbx+r12], 0
    mov rax, rbx
    mov rdx, r12
.cleanup:
    pop r14
    pop r13
    pop r12
    pop rbx
    ret
.buffer_overflow:
    xor eax, eax
    jmp .cleanup

; Принимает адрес начала буфера, размер буфера, файловый дескриптор
; Читает в буфер слово из stdin, пропуская пробельные символы в начале
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx
; Дописывает к слову нуль-терминатор
; При неудаче возвращает 0 в rax
read_line:
%macro JUMP_IF_ENDLINE 1
    cmp al, C_NEWLINE
    je %1
    test al, al
    jz %1
%endmacro
    push rbx
    mov rbx, rdi
    push r12
    xor r12d, r12d
    push r13
    mov r13, rsi
    push r14
    mov r14, rdx
    cmp r12, r13
    je .buffer_overflow
.read_and_check_next:
    mov rdi, r14
    call read_char
    JUMP_IF_ENDLINE .done
    mov [rbx+r12], al
    inc r12
    cmp r12, r13
    jne .read_and_check_next
.buffer_overflow:
    xor eax, eax
.cleanup:
    pop r14
    pop r13
    pop r12
    pop rbx
    ret
.done:
    mov byte [rbx+r12], 0
    mov rax, rbx
    mov rdx, r12
    jmp .cleanup


; Принимает указатель на строку, пытается прочитать из её начала беззнаковое число
; Возвращает в rax: число, rdx: его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor edx, edx
    xor eax, eax
    xor esi, esi
    xor ecx, ecx
.check_next_char:
    mov sil, [rdi+rcx]
    cmp sil, '9'
    ja .done
    sub sil, '0'
    jb .done
    inc rcx
    lea rax, [rax+rax*4]    ; gcc optimized way 
    add rax, rax            ; to multiply by 10
    jo .format_overflow
    add rax, rsi
    jnc .check_next_char
.format_overflow:
    xor eax, eax
    xor edx, edx
    ret
.done:
    mov rdx, rcx
    ret

; Принимает указатель на строку, пытается прочитать из её начала знаковое число
; Если есть знак, пробелы между ним и числом не разрешены
; Возвращает в rax: число, rdx: его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte [rdi], '-'
    jne .parse_positive
.parse_negative:
    inc rdi
    call parse_uint
    test rdx, rdx
    jz .unsigned_parse_failure
    neg rax
    jns .signed_overflow
    inc rdx
    ret
.parse_positive:
    call parse_uint
    test rdx, rdx
    jz .unsigned_parse_failure
    test rax, rax
    js .signed_overflow
    ret
.signed_overflow:
    xor edx, edx
.unsigned_parse_failure:
    xor eax, eax
    ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    push rdi
    push rsi
    push rdx
    call string_length
    pop rdx
    pop rsi
    pop rdi
    cmp rax, rdx
    jnb .buffer_overflow
    mov rcx, rax
.copy_next_char:
    mov r8b, [rdi + rcx]
    mov [rsi + rcx], r8b
    sub rcx, 1
    jnc .copy_next_char
.done:
    ret
.buffer_overflow:
    xor eax, eax        
    ret
