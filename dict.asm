section .text
extern string_equals
global find_word

; Принимает указатель на нуль-терминированную строку и указатель на начало словаря.
; Если существует вхождение ключа в словарь, возвращает его адрес, иначе 0.
find_word:
    push r12
    mov r12, rdi
    push rbx
    mov rbx, rsi
.check_next_item:
    test rbx, rbx
    jz .not_found
    mov rdi, r12
    mov rsi, [rbx + 8]
    call string_equals
    test rax, rax
    jnz .done
    mov rbx, [rbx]
    jmp .check_next_item
.not_found:
.done:
    mov rax, rbx
    pop rbx
    pop r12
    ret