%include "words.inc"

section .data
error_key_not_found db "ERROR: key is not found", 0
error_buffer_overflow db "ERROR: buffer overflow", 0
%define BUFFER_SIZE 256

section .text
extern FD_STDIN
extern FD_STDOUT
extern FD_STDERR
extern read_line
extern find_word
extern parse_int
extern print_string
extern string_equals
extern exit

global _start
_start:
    sub rsp, BUFFER_SIZE
    mov rdi, rsp
    mov rsi, BUFFER_SIZE
    mov rdx, FD_STDIN
    call read_line
    test rax, rax
    jz .buffer_overflow
    mov rdi, rax
    mov rsi, dict
    call find_word
    test rax, rax
    jz .not_found
    mov rdi, [rax + 16]
    mov rsi, FD_STDOUT
    call print_string
    xor edi, edi
.cleanup:
    add rsp, BUFFER_SIZE
    call exit
.buffer_overflow:
    mov rdi, error_buffer_overflow
    jmp .error
.not_found:
    mov rdi, error_key_not_found
.error:
    mov rsi, FD_STDERR
    call print_string
    mov edi, 1
    jmp .cleanup
