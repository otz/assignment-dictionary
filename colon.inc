%define dict 0
%macro colon 2
    %%new_head: dq dict, %%key, %2
    %define dict %%new_head
    %%key: db %1, 0
    %2:
%endmacro